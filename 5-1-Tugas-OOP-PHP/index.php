<?php 

 trait Hewan {
   public $nama;
   public $darah = 50;
   public $jumlahkaki;
   public $keahlian;

   public function atraksi(){
     echo "{$this->nama} sedang {$this->keahlian}";
   }
  }

 abstract class Fight {
   use Hewan;
   public $attackPower;
   public $defensePower;
   
    public function serang($hewan){
     echo "{$this->nama} sedang menyerang {$hewan->nama}";
     echo "<br>";
     $hewan->diserang($this);
    }

    public function diserang($hewan){
     echo "{$this->nama} sedang diserang {$hewan->nama}";

     $this->darah = $this->darah - ($hewan->attackPower / $this->defensePower) ;
    }

    protected function getinfo(){
     echo "Nama : {$this->nama}";
     echo "<br>";
     echo "Jumlah Kaki : {$this->jumlahkaki}";
     echo "<br>";
     echo "Keahlian : {$this->keahlian}";
     echo "<br>";
     echo "Darah : {$this->darah}";
     echo "<br>";
     echo "Attack Power : {$this->attackPower}";
     echo "<br>";
     echo "Defense Power : {$this->defensePower}";
     echo "<br>";
    }
    abstract public function getinfoHewan();
  }

 class Elang extends Fight {
   public function __construct($nama){
      $this->nama = $nama;
      $this->jumlahkaki = 2;
      $this->keahlian = "terbang tinggi";
      $this->attackPower = 10;
      $this->defensePower = 5;
   }

    public function getinfoHewan(){
     echo "Jenis Hewan : Elang";
     echo "<br>";
     $this->getinfo();
    }
  }

 class Harimau extends Fight {
    public function __construct($nama){
      $this->nama = $nama;
      $this->jumlahkaki = 4;
      $this->keahlian = "berlari dengan cepat";
      $this->attackPower = 8;
      $this->defensePower = 7;
    }
    public function getinfoHewan(){
      echo "Jenis Hewan : Harimau";
      echo "<br>";
      $this->getinfo();
    }
  }

    

  class Spasi {
    public static function tampilkan(){
      echo "<br>";
      echo "==================";
      echo "<br>";
      echo "<br>";
    }
  }

  $harimau = new Harimau("Harimau Kalimantan");
  $harimau->getinfoHewan();
  Spasi::tampilkan();
  $elang = new Elang("Elang Sumatera");
  $elang->getinfoHewan();
  Spasi::tampilkan();
  $elang->serang($harimau);
  Spasi::tampilkan();
  $harimau->getinfoHewan();
?>
