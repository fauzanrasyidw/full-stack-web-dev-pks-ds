//soal 1
// const LuasPersegi = (panjang, lebar) => {
//   console.log(`Luasnya, ` + panjang * lebar, `& Kelilingnya, ` + (2 * (panjang + lebar)));
// }

// LuasPersegi(10, 5);

//soal 2
// const newFunction = (firstName, lastName) => {
//   fullName = function () {
//     console.log(firstName + " " + lastName)
//   }
//   return { firstName, lastName, fullName }
// }
// newFunction("William", "Imoh").fullName()

//soal 3
// const newObject = {
//   firstName: "Muhammad",
//   lastName: "Iqbal Mubarok",
//   address: "Jalan Ranamanyar",
//   hobby: "playing football",
// }

// const { firstName, lastName, address, hobby } = newObject

// console.log(firstName, lastName, address, hobby)

//soal 4
// let west = ["Will", "Chris", "Sam", "Holly"]
// let east = ["Gill", "Brian", "Noel", "Maggie"]
// let combined = [...west, ...east]
// console.log(combined)

//soal 5
// const planet = "earth"
// const view = "glass"
// const theString = `Lorem, ${view} , dolor sit amet, consectetur adipiscing elit, ${planet}`
// console.log(theString)
