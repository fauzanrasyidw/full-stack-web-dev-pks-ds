<?php

namespace App\Http\Controllers;

use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $role = Roles::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Rolenya',
            'data'    => $role  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
    
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $role = Roles::create([
            'name'     => $request->name,
        ]);

    
        if($role) {

            return response()->json([
                'success' => true,
                'message' => 'Roles Created',
                'data'    => $role 
            ], 201);

        } 

        return response()->json([
            'success' => false,
            'message' => 'Roles Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Roles::findOrfail($id);

        
        return response()->json([
            'success' => true,
            'message' => 'Roles Data Post',
            'data'    => $role 
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required',
        ]);
        
     
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

       
        $role = Roles::findOrFail($id);

        if($role) {

           
            $role->update([
                'name'     => $request->name,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Name Updated',
                'data'    => $role  
            ], 200);

        }

    
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Roles::findOrfail($id);

        if($role) {

        
            $role->delete();

            return response()->json([
                'success' => true,
                'message' => 'Role Deleted',
            ], 200);

        }

    
        return response()->json([
            'success' => false,
            'message' => 'Role Not Found',
        ], 404);
    }
}
