<?php

namespace App\Http\Controllers;

use App\User;
use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PostController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Post::latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'List Data Postnya',
            'data'    => $post  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
    
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();

        $post = Post::create([
            'title'     => $request->title,
            'description'   => $request->description,
            'user_id'  => $user->id
        ]);

    
        if($post) {

            return response()->json([
                'success' => true,
                'message' => 'Post Created',
                'data'    => $post  
            ], 201);

        } 

        return response()->json([
            'success' => false,
            'message' => 'Post Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
          $post = Post::findOrfail($id);

        
          return response()->json([
              'success' => true,
              'message' => 'Detail Data Post',
              'data'    => $post 
          ], 200);

  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'description' => 'required',
        ]);
        
     
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

       
        $post = Post::find($id);

        if($post) {

            $user = auth()->user();

            if($post->user_id == $user->id)
            {     
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post bukan milik user',
                    'data'    => $post 
                ], 403);
            }
           
            $post->update([
                'title'     => $request->title,
                'description'   => $request->description
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Post Updated',
                'data'    => $post 
            ], 200);

        }

    
        return response()->json([
            'success' => false,
            'message' => 'Post Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         
         $post = Post::findOrfail($id);

         if($post) {
            
            $user = auth()->user();

            if($post->user_id == $user->id)
            {     
                return response()->json([
                    'success' => false,
                    'message' => 'Data Post bukan milik user',
                    'data'    => $post 
                ], 403);
            }
 
         
             $post->delete();
 
             return response()->json([
                 'success' => true,
                 'message' => 'Post Deleted',
             ], 200);
 
         }
 
     
         return response()->json([
             'success' => false,
             'message' => 'Post Not Found',
         ], 404);
     
    }
}
