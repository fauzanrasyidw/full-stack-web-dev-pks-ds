<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommentController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth:api')->only(['store', 'update', 'delete']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $post_id = $request ->post_id;

        $comments = Comment::where('post_id', $post_id)->latest()->get();

        return response()->json([
            'success' => true,
            'message' => 'data commentnya disini',
            'data'    => $comments  
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allRequest = $request->all();
       
        $validator = Validator::make($allRequest, [
            'content'   => 'required',
            'post_id'   => 'required'
        ]);
        
    
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = auth()->user();

        $comment = Comment::create([
            'content'     => $request->content,
            'post_id'     => $request->post_id,
            'user_id'     => $user->id
        ]);

    
        if($comment) {

            $user = auth()->user();

            if($comment->user_id == $user->id)
            {     
                return response()->json([
                    'success' => false,
                    'message' => 'Data Comment bukan milik user',
                ], 403);
            }

            return response()->json([
                'success' => true,
                'message' => 'Comment Created',
                'data'    => $comment 
            ], 201);

        } 

        return response()->json([
            'success' => false,
            'message' => 'Comment Failed to Save',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $comment = Comment::find($id);

        
        return response()->json([
            'success' => true,
            'message' => 'Comment Data Post',
            'data'    => $comment 
        ], 200);

        return response()->json([
            'success' => false,
            'message' => 'Comment id : ' . $id . 'Not Found',
        ], 404);
    
    }

   
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allRequest = $request->all(); 

        $validator = Validator::make($allRequest, [
            'content'   => 'required'
        ]);
        
     
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

       
        $comment = Comment::findOrFail($id);

        if($comment) {

            $user = auth()->user();

            if($comment->user_id == $user->id)
            {     
                return response()->json([
                    'success' => false,
                    'message' => 'Data Comment bukan milik user',
                ], 403);
            }

           
            $comment->update([
                'content'     => $request->content
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Comment Updated',
                'data'    => $comment
            ], 200);

        }

    
        return response()->json([
            'success' => false,
            'message' => 'Comment Not Found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comment = Comment::findOrfail($id);

        if($comment) {

            $user = auth()->user();

            if($comment->user_id == $user->id)
            {     
                return response()->json([
                    'success' => false,
                    'message' => 'Data Comment bukan milik user',
                ], 403);
            }

        
            $comment->delete();

            return response()->json([
                'success' => true,
                'message' => 'Comment Deleted',
            ], 200);

        }

    
        return response()->json([
            'success' => false,
            'message' => 'comment Not Found',
        ], 404);
    }
}
