<?php

namespace App\Listeners;

use App\OtpCode;
use App\Mail\OtpCodeMail;
use Illuminate\Support\Facades\Mail;
use App\Events\OtpCodeStoreEvent;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendEmailtoUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OtpCodeStoreEvent  $event
     * @return void
     */
    public function handle(OtpCodeStoreEvent $event)
    {
        Mail::to($event->otpCode->user->email)->send(new OtpCodeMail($event->otpCode , $event->newUserStatus));
    }
}
